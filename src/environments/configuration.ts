import {Images} from '../interface/images'

export interface Configuration {
    images: Images;
    change_keys: string[];
}


