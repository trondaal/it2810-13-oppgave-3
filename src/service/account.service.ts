import {Injectable} from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {ReplaySubject} from "rxjs";

@Injectable()
export class AccountService {
    private uid: any;

    constructor(private af: AngularFire) {
    }


    /**
     * Creates user object and remembers useriD for further use.
     *
     * @param user - Value sent from Angularfire's Auth Subscription
     * @returns {}
     */
    public getUserInfo(user: any): any {
        if(!user) {
            return {};
        }
        let data = user.auth.providerData[0];
        this.uid = user.uid;
        return {
            name: data.displayName,
            avatar: data.photoURL,
            email: data.email,
            uid: user.uid,
            provider: data.providerId
        };
    }

    /**
     * Sets query to userspesific log in firebase
     * @param query: Search query sent from MovieService
     */
    public addToHistory(query: string): void{
        const history = this.af.database.list(`users/${this.uid}/log`);
        history.push(query);
    }

    /**
     * FirebaseListObservable so components can subscribe to authUser database
     * @returns {FirebaseListObservable<any[]>}
     */
    public getHistory(): FirebaseListObservable<any>{
        return this.af.database.list(`users/${this.uid}/log`,{ query: {
            'limitToLast': 10
        }});
    }

    /**
     * Deletes authUsers query log
     */
    public clearLog(){
        this.af.database.list(`users/${this.uid}/log/`).remove()
    }

    /**
     * Deletes single firebase object,
     * @param key - firebase $key to identify object
     */
    public delete(key: string){
        this.af.database.object(`users/${this.uid}/log/${key}`).remove()
    }
}