import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import {Subscribable} from "rxjs/Observable";
import {Movie} from "../interface/movie";
import {MovieService} from "./movie.service";
import {Observable, ReplaySubject} from "rxjs";
import {take} from "rxjs/operator/take";


@Injectable()
export class SearchService{
    public movieList: ReplaySubject<Movie[]> = new ReplaySubject<Movie[]> (1);
    public inputControls = new InputControls();
    private localMovieList: Movie[];
    deletedEigthiesMovies: Movie[] = [];
    deletedTwoThousandMovies: Movie[] = [];
    deletedTwoThousandTenMovies: Movie[] = [];
    deletedActionMovies: Movie[] = [];
    deletedComedyMovies: Movie[] = [];
    deletedHorrorMovies: Movie[] = [];
    query: string;
    page: number = 1;
    totalPages: number;
    finnished: boolean = true;
    public selectedMovie: Movie;

    constructor (private ms: MovieService) {
        this.movieList.subscribe( data => {
            this.localMovieList = data
        })
    }

    getPlaying(){
        this.ms.getNowPlaying().subscribe(data => {
                this.localMovieList = data.results;
                this.setAnimationStateInactive();
            }
        );
    }

    search(searchQuery: string){
        if (searchQuery != ""){
            this.query = searchQuery;
            this.ms.searchTheMovieDB(searchQuery)
                .subscribe(
                    data => {
                        this.localMovieList = data.results;
                        this.totalPages = data.total_pages;
                        this.setAnimationStateInactive();
                        this.testCheckboxes();
                        this.checkEnoughMovies();
                    }
                );
        }else{
            this.getPlaying();
        }
        this.selectedMovie = null;
    }

    setAnimationStateInactive(){
        //Sets all movie's
        for (let movie of this.localMovieList ){
            movie.animationState = 'inactive';
        }
        //Saves changes done to the subject emitter.
        this.movieList.next(this.localMovieList);
    }

    //Deletes movies from movieList by genre and adds them to deletedMovies
    filterMoviesGenre(genreID: number, deletedMovies: Movie[]){
        for (let i = this.localMovieList.length - 1; i >= 0; i--){
            if (this.localMovieList[i].genre_ids.indexOf(genreID) < 0){
                deletedMovies.push(this.localMovieList[i]);
                this.localMovieList.splice(i, 1)
            }
        }
        this.checkEnoughMovies();
        this.movieList.next(this.localMovieList);
    }

    //Filter movies from movieList by year and adds them to deletedMovies
    filterMoviesYear(year: number, deletedMovies: Movie[]){
         for (let i = this.localMovieList.length - 1; i >= 0; i--){
                if (parseInt(this.localMovieList[i].release_date.split("-")[0]) < year){
                    deletedMovies.push(this.localMovieList[i]);
                    this.localMovieList.splice(i, 1)
                }
            }
        this.checkEnoughMovies();
        // this.checkEnoughMovies();
            this.movieList.next(this.localMovieList);
    }



    //Restores movies from deletedMovies
    restoreMovies(deletedMovies: Movie[]){
        for (let i = deletedMovies.length - 1; i >= 0; i--){
            this.localMovieList.push(deletedMovies[i]);
            deletedMovies.splice(i,1)
        }
        this.movieList.next(this.localMovieList);


    }

    checkEnoughMovies(){
        if (this.localMovieList.length < 20){
            while (this.page < this.totalPages && this.finnished){
                this.loadMore();
            }
        }
    }
    // Loads more movies into movieList
    loadMore(){
        this.page += 1;
        this.finnished = false;
        if(this.query == null){
            this.ms.getNowPlaying(this.page.toString()).subscribe( data => {
                    for (let movie of data.results){
                        this.localMovieList.push(movie)
                    }
                    this.setAnimationStateInactive();
                    this.finnished = true;
                    this.testCheckboxes();
                }
            )
        }
        else {
            this.ms.searchTheMovieDB(this.query,  this.page.toString()).subscribe(data => {
                for (let movie of data.results){
                    this.localMovieList.push(movie)
                }
                this.setAnimationStateInactive();
                this.finnished = true;
                this.testCheckboxes();
            })
        }
    }


    /*This is the sorting coordinator that sorts based on checked checkboxes.
     * If 0, sort alpha. If 1, sort rating.
     * If 0 and 1, then sort alpha+rating on each other.*/
    sort_list(category: number){
        if(category == 0){
            this.localMovieList.sort(this.compareTitle);
        }
        else{
            this.localMovieList.sort(this.compareRating);
        }
        this.movieList.next(this.localMovieList);
    }

    /*Comparator that returns the correctly sorted value based on alphabetical value*/
    compareTitle(a, b) {
        if (a.title < b.title)
            return -1;
        if (a.title > b.title)
            return 1;
        return 0;
    }

    /*Comparator that returns the correctly sorted value based on rating*/
    compareRating(a, b){
        if (a.vote_average > b.vote_average)
            return -1;
        if (a.vote_average < b.vote_average)
            return 1;
        return 0;
    }
    //Checks if the checkboxes are checked
    testCheckboxes(){
        if (this.inputControls.action){
            this.filterMoviesGenre(18, this.deletedActionMovies)
        }else {this.restoreMovies(this.deletedActionMovies)}
        if (this.inputControls.comedy){
            this.filterMoviesGenre(35, this.deletedComedyMovies)
        }else {this.restoreMovies(this.deletedComedyMovies)}
        if (this.inputControls.horror){
            this.filterMoviesGenre(27, this.deletedHorrorMovies)
        }else {this.restoreMovies(this.deletedHorrorMovies)}

        if (this.inputControls.eighties){
            this.filterMoviesYear(1980, this.deletedEigthiesMovies)
        }else {this.restoreMovies(this.deletedEigthiesMovies)}
        if (this.inputControls.twoThousand){
            this.filterMoviesYear(2000, this.deletedTwoThousandMovies)
        }else {this.restoreMovies(this.deletedTwoThousandMovies)}
        if (this.inputControls.twoThousandTen){
            this.filterMoviesYear(2010, this.deletedTwoThousandTenMovies)
        }else {this.restoreMovies(this.deletedTwoThousandTenMovies)}

      if (this.inputControls.sorting == "rating"){
        this.sort_list(1)
      }else {
        this.sort_list(0)
      }
    }

}

export class InputControls {
    sorting = "rating";
    action = false;
    comedy = false;
    horror = false;
    eighties = false;
    twoThousand = false;
    twoThousandTen = false;

}
