/**
 * Created by mathiafl on 25.10.16.
 */
import { Injectable } from "@angular/core";
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { TmdbSettings} from '../environments/tmdb-settings';
import {AccountService} from "./account.service";


@Injectable()
export class MovieService{


  private http: Http;
  public settings: TmdbSettings;

  constructor (http: Http, private as: AccountService) {
      this.http = http;
      this.settings = {
          baseUrl: 'https://api.themoviedb.org/3',
          apiKey: 'api_key=061290ec7f5b488383aba3a0f472e54c'

      }
  }

  init() {
      this.http.get(this.tmdbAction('/configuration', " "))
          .map(res => res.json())
          .subscribe(
              function(data){
                console.log(data);
              },
              () => console.log("getConfiguration complete..."));
 }


  getNowPlaying(page?: string) {
      let query = '';
      if(page){
          query = "&page=" + page;
      }
      return this.http.get(this.tmdbAction('/movie/top_rated', query)).map(res => res.json());
  }

  //Takes in a query string and returns a result from TheMovieDB
  searchTheMovieDB(query: string, page?: string){
      let queryString = "&query=" + query;
      if (page) {
          queryString += "&page=" + page;
      }else {
          this.as.addToHistory(query);
      }
    return this.http.get(this.tmdbAction('/search/movie', queryString)).map(res => res.json());
  }

  private tmdbAction(action: string, query: string) : string {
      return `${this.settings.baseUrl}${action}?${this.settings.apiKey}${query}`
  }
}
