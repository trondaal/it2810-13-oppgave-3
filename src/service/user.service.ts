import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import { AngularFire, AuthProviders } from 'angularfire2';


@Injectable()
export class UserService {
    public redirectUrl: string;

    constructor(
        private af: AngularFire,
        private router: Router) {
    }


    login(from: string){
        this.af.auth.login({
            provider: UserService._getProvider(from)
        }).then(value =>
            console.log(value)
        )};

    logout() {
        this.af.auth.logout();
    }

    static _getProvider(from: string) {
        switch(from){
            case 'github': return AuthProviders.Github;
            case 'google': return AuthProviders.Google;
        }
    }
}
