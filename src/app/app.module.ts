import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { Http,HttpModule} from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AngularFireModule, AuthMethods } from 'angularfire2';

import { AccountService } from '../service/account.service';
import { AppComponent }   from './app.component';
import  { MovieService } from '../service/movie.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import {FancyComponent} from './fancy/fancy.component';
import { UserService } from '../service/user.service';
import { AuthGuard } from '../service/auth-guard.service';
import { firebaseConfig } from '../environments/firebase.config';
import { AppRoutingModule }     from './app-routing.module';
import { AuthComponent } from './auth/auth.component';
import { AccountHistoryComponent } from './account-history/account-history.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import {SearchService} from "../service/search.service";

@NgModule({
  imports:  [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    MaterialModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig,
    {
      method: AuthMethods.Redirect
    })
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    FancyComponent,
    AuthComponent,
    AccountHistoryComponent,
    SearchBarComponent
  ],
  providers:  [
    AuthGuard,
    UserService,
    MovieService,
    AccountService,
    SearchService
  ],
  bootstrap:  [
      AppComponent
  ]
})


export class AppModule { }
