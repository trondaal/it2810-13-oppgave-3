import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../service/user.service';
import { AngularFire } from 'angularfire2';

@Component({
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']

})

export class AuthComponent {
  constructor(
    private us: UserService,
    private af: AngularFire,
    private router: Router) {
      this.af.auth.subscribe( res => {
          if(res){
              let redirect = this.us.redirectUrl ? this.us.redirectUrl : '/dashboard';
              this.router.navigate([redirect])
          }
      })
  }

    login(provider: string) {
      this.us.login(provider);
    }
    

}
