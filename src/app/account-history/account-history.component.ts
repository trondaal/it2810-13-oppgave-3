import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {AccountService} from "../../service/account.service";
import {FirebaseListObservable} from "angularfire2";

@Component({
  selector: 'app-account-history',
  templateUrl: './account-history.component.html',
  styleUrls: ['./account-history.component.css']
})
export class AccountHistoryComponent implements OnInit {
  history: {};
  @Output() searched = new EventEmitter();

  constructor(private as: AccountService) { }

  ngOnInit() {
    this.as.getHistory().subscribe(
        data => {
          this.history = data.reverse();
        }
    );
  }

  search(query: any, key: string){
    this.delete(key);
    this.searched.emit(query);
  }

  removePornSearch(){
      this.as.clearLog();
  }

  delete(key: string){
    this.as.delete(key);
  }

}
