import { Component, OnInit} from '@angular/core';
import { Movie } from "../../interface/movie";
import {SearchService} from "../../service/search.service";

@Component({
    selector: 'my-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.component.css']
})

export class DashboardComponent implements OnInit{
    movieList: Movie[];

    constructor(private ss: SearchService
      ){ }

    ngOnInit(){
        this.ss.movieList.subscribe( data => {
            this.movieList = data
        });
        this.ss.getPlaying();
    }

    onSelect(movie: Movie): void {
        this.ss.selectedMovie = movie;
    }
}



