import { Component, OnInit, trigger, state, style, transition, animate} from '@angular/core';
import { Movie } from "../../interface/movie";
import {SearchService} from "../../service/search.service";

@Component({
  selector: 'fancy',
  templateUrl: './fancy.component.html',
  styleUrls: ['./fancy.component.css'],

  animations: [
    //Fancy movieposter animation
    trigger('animate', [
      state('inactive', style({
        backgroundColor: '#eee',
        transform: 'translate3d(0, 0, 0)'
      })),
      state('active', style({
        backgroundColor: '#cfd8dc',
        transform: 'translate3d(-90%, 0, 0)'
      })),
      transition('inactive => active', animate('500ms ease-in')),
      transition('active => inactive', animate('500ms ease-out')),
    ]),

    //Fancy movie description animation
    trigger('text', [
      state('inactive', style({
        opacity: 0.0,
        transform: 'translate3d(0, 0, 0)',
      })),
      state('active', style({
        opacity: 1.0,
        transform: ('translate3d(90%, 0, 0)', 'translateX(110%) scale(1)')
      })),
      transition('inactive => active', animate('500ms ease-in')),
      transition('active => inactive', animate('500ms ease-out')),
    ])
  ]
})
export class FancyComponent implements OnInit {
  movieList: Movie[];

  constructor(private ss: SearchService){

  }

  ngOnInit(){
    this.ss.movieList.subscribe( data => {
      this.movieList = data
    });
    this.ss.getPlaying();
  }

  //toggles the state of the movies for animation
  toggleState(movie){
    movie.animationState = (movie.animationState === 'active' ? 'inactive' : 'active');
  }
}

