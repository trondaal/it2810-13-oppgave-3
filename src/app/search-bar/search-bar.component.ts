import { Component, HostListener, Inject} from '@angular/core';
import { DOCUMENT } from "@angular/platform-browser";
import {SearchService} from "../../service/search.service";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})

export class SearchBarComponent {

  constructor(private ss: SearchService,
              @Inject(DOCUMENT) private document: Document) { }

  //Takes in input from searchField, makes a search request at TheMovieDB, then updates movieList with results
  search(searchQuery: string): void {
    this.ss.search(searchQuery);
  }

  //Checks if user has scrolled to bottom of page
  @HostListener("window:scroll", [])
  onWindowScroll() {
    if((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 50) {
      this.ss.loadMore();
    }
  }
}

