import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../service/auth-guard.service';
import { AuthComponent } from './auth/auth.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import {FancyComponent} from "./fancy/fancy.component";
import {SearchBarComponent} from "./search-bar/search-bar.component";

const routes: Routes = [
    {   path: '', component: SearchBarComponent,
        canActivate: [AuthGuard],
        children: [
            {   path: 'dashboard', component: DashboardComponent},
            {   path: 'fancy', component: FancyComponent }
        ]

    },
    {   path: 'login',  component: AuthComponent},


];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
