import { Component,OnInit } from "@angular/core";
import {DashboardComponent} from './dashboard/dashboard.component';
import { Router } from '@angular/router';
import { AccountService } from '../service/account.service';

import { AngularFire, AuthProviders, FirebaseAuth } from 'angularfire2';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html' ,
    styleUrls: [ 'app.component.css']
})
export class AppComponent implements OnInit {
    title = 'MovieBook';
    user = {};
     constructor(private af: AngularFire, private router: Router, private as: AccountService) {
  }

    ngOnInit() {
        this.af.auth.subscribe(
            user => {
                this.user = this.as.getUserInfo(user);
                if (!user) {
                    this.router.navigate(['/login']);
                }
            },
            error => console.trace(error)
            );
  }

  logout() {
    this.af.auth.logout();
  }
}
