
/**
 * Created by mathiafl on 21.10.16.
 */
export interface Movie {
    adult?                : boolean;
    backdrop_path?        : string;
    belongs_to_collection?: any;
    budget?               : number;
    homepage?             : string;
    id                    : number;
    imdb_id?              : number;
    original_title?       : string;
    overview?             : string;
    popularity?           : number;
    poster_path?          : string;
    release_date?         : string;
    revenue?              : number;
    runtime?              : number;
    status?               : string;
    tagline?              : string;
    title                 : string;
    vote_average?         : number;
    vote_count?           : number;
    genre_ids             : number[];
    animationState        : string;
}
