# Project description
Our site, MovieBook, is a movie database that is used tmdb(TheMovieDatabase). This project is purely based on learning angular development, and we therefore don't reserve any rights towards the movies.

# Project Structure
The project has been split into two parts: The server part and the client part.

## Server
This is the backbone of our webapp. Source code link:

## Client
The client is written using Angular2 typescript. Documentation link: 

# How to set up test-server:

Install node.js
Open commandline and navigate to project folder
Type npm update in commandline
Type npm start in commandline
Navigate to http://localhost:4200/ in your browser

# Angular

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.18.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).